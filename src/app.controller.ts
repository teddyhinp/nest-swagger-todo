import { Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { Todo } from 'type';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/todo')
  getTodo(): Todo[] {
    return this.appService.getTodo();
  }

  @Post('/todo')
  createTodo(): Todo[] {
    return this.appService.createTodo();
  }
}
