import { Injectable } from '@nestjs/common';
import { Todo } from 'type';

@Injectable()
export class AppService {
  getTodo(): Todo[] {
    return [
      { id: 1, content: 'new todo' },
      { id: 2, content: 'new todo2' },
    ];
  }
  createTodo(): Todo[] {
    return [
      { id: 1, content: 'new todo' },
      { id: 2, content: 'new todo2' },
    ];
  }
}
